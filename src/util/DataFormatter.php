<?php

namespace Util;

use DateTime;

class DataFormatter
{    
    //Criar os métodos estáticos -- evitando assim a criação de objeto da classe DataFormatter
    public static function mySqlToBr($data)
    {

        $date = new DateTime($data);

        if (strlen($data) > 10) {
            return $date->format('d/m/Y H:i:s');
        }

        return $date->format('d/m/Y');
    }

    public static function brToMySql($data)
    {
        
        $data = str_replace("/", "-", $data);
        
        $date = new DateTime($data);

        if (strlen($data) > 10) {
            return $date->format('Y-m-d H:i:s');
        }

        return $date->format('Y-m-d');
    }
}