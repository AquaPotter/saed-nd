<?php

namespace Util;

class Upload {

    //Atributos 
    //Local de armazenamento
    protected $caminhoArmazenamento = null;
    //Lista de tipos de arquivo permitidos
    protected $extensoesPermitidas = array();

    protected $files = null;

    //Método construtor

    public function __construct($local, $listaPermitidos,$files) {
        $this->caminhoArmazenamento = $local;
        $this->extensoesPermitidas = $listaPermitidos;
        $this->files = $files;
    }

    //Métodos GET e SET

    public function getCaminhoArmazenamento() {
        return $this->caminhoArmazenamento;
    }

    public function setCaminhoArmazenamento($caminho) {
        $this->caminhoArmazenamento = $caminho;
    }

    public function getExtensoesPermitidas() {
        return $this->extensoesPermitidas;
    }

    public function setExtensoesPermitidas($lista) {
        $this->extensoesPermitidas = $lista;
    }

    //Métodos

    public function upload() {
        //responsável por realizar o upload --> mover o arquivo para o local desejado 
        $arquivo = $_FILES[$this->files];
        if ($this->validaDados() === true) {
            $diretorioLocal = getcwd();
            $pastaDestino = $diretorioLocal . DIRECTORY_SEPARATOR . $this->caminhoArmazenamento;
            //representa o caminho completo do arquivo até a pasta de destino
            $arquivoUpload = $pastaDestino . DIRECTORY_SEPARATOR . $arquivo["name"];
            if (move_uploaded_file($arquivo["tmp_name"], $arquivoUpload)) {
                return true;
            } else {
                return 11;
            }
        } else {
            return $this->validaDados();
        }
    }

    public function validaDados() {
        //validar 
        // se o arquivo realmente foi upado - ok
        // se já existe arquivo com o mesmo nome - ok
        // o tipo de arquivo - ok
        // $_FILES -- erros
        $arquivo = $_FILES[$this->files];

        if ($arquivo['error'] == 0) {
            //não tem erro arquivo upado com sucesso
            //montar o caminho até a pasta de destino do upload
            $diretorioLocal = getcwd();
            //montando caminho harcoded 
            // $pastaDestino = $diretorioLocal . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "upload";
            //montar caminho  utilizando o atributo;
            $pastaDestino = $diretorioLocal . DIRECTORY_SEPARATOR . $this->caminhoArmazenamento;

            //representa o caminho completo do arquivo até a pasta de destino
            $arquivoUpload = $pastaDestino . DIRECTORY_SEPARATOR . $arquivo["name"];

            if (!file_exists($arquivoUpload)) {
                //não existe arquivo com o mesmo nome

                if (in_array($arquivo['type'], $this->extensoesPermitidas)) {
                    //extensao permitida                
                    return true;
                } else {
                    //extensão proibida
                    return 10;
                }
            } else {
                return 9;
            }
            
            
            /*
             * 
              if (file_exists($arquivoUpload)) {
              // existe arquivo com o mesmo nome, neste caso alteramos o nome do arquivo
                    
                contar caracteres 
                $tamanho =  strlen($arquivo['name']);
             
                 $arquivo['name'] = substr($arquivo['name'],0,$tamanho-4) . "1". substr($arquivo['name'],$tamanho-4,4)  ;
             
               if (in_array($arquivo['type'], $this->extensoesPermitidas)) {
              //extensao permitida
              return true;
              } else {
              //extensão proibida
              return 10;
              }
              }
             * 
             */
        } else {
            //Arquivo não chegou ao servidor com sucesso ver codigos de erro abaixo
            return $arquivo['error'];
        }
    }

}

/*

[name] => MyFile.jpg
[type] => image/jpeg
[tmp_name] => /tmp/php/php6hst32
[size] => 98174
[error] => UPLOAD_ERR_OK
 
 * 
 * 
 * *  * 
UPLOAD_ERR_OK

    Value: 0; There is no error, the file uploaded with success.
UPLOAD_ERR_INI_SIZE

    Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
UPLOAD_ERR_FORM_SIZE

    Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
UPLOAD_ERR_PARTIAL

    Value: 3; The uploaded file was only partially uploaded.
UPLOAD_ERR_NO_FILE

    Value: 4; No file was uploaded.
UPLOAD_ERR_NO_TMP_DIR

    Value: 6; Missing a temporary folder. Introduced in PHP 5.0.3.
UPLOAD_ERR_CANT_WRITE

    Value: 7; Failed to write file to disk. Introduced in PHP 5.1.0.
UPLOAD_ERR_EXTENSION

    Value: 8; A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.


 
    9 - indica duplicidade de nomes de arquivo

    10  - indica uma extensão proibida
    
    11 - falha ao mover o arquivo
 
  
 */