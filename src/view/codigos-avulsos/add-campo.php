<h1>Adicionar Campo</h1>
        <form>
            <div id="formulario">
                <div class="form-group">
                    <label>Aula: </label>
                    <input type="text" name="titulo[]" placeholder="Nome da aula">
                    <button type="button" id="add-campo"> + </button>
                </div>
            </div>
            <div class="form-group">
                <input type="button" value="Cadastrar">
            </div>
        </form>

        <script>
            //https://api.jquery.com/click/
            $("#add-campo").click(function () {
				//https://api.jquery.com/append/
                $("#formulario").append('<div class="form-group"><label>Aula: </label> <input type="text" name="titulo[]" placeholder="Nome da aula"></div>');
            });
        </script>