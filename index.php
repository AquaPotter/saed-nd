
<html lang=pt-br>
    <head>
        <title>SAED NEW DIRECTIONS</title>
        <meta charset=UTF-8> 
        <meta name=viewport content="width=device-width, initial-scale=1.0">
        <meta name=description content="">
        <meta name=keywords content="">
        <meta name=author content=''>
        <link rel="icon" href="">
        <link href="public/css/default.css" rel="stylesheet">
        <!-- Telas Responsivas -->
        <link rel="stylesheet" media="screen and (max-width:480px)" href="public/css/style480.css">
        <link rel="stylesheet" media="screen and (min-width:481px) and (max-width:768px)" href="public/css/style768.css">
        <link rel="stylesheet" media="screen and (min-width:769px) and (max-width:1024px)" href="public/css/style1024.css">
        <link rel="stylesheet" media="screen and (min-width:1025px) and (max-width:1366px)" href="public/css/style1025.css">    
        <script type="text/javascript" src="public/js/script.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <div id=container>
            <header>
                <?PHP
                require_once 'view/cabecalho.php';
                
                ?>
            </header>
            <main>                                        
                <?PHP
                if (isset($_GET['p'])){
                     $exemplo = $_GET['p'];
                    require_once './view/' . $exemplo . '.php';
                } else {
                    require_once 'view/home.php';
                }
                
                ?>
            </main>
            <footer>
               <?PHP
                require_once 'view/rodape.php';
                
                ?>
            </footer>
        </div>
        
        
        <script src="public/js/jquery.js" type="text/javascript"></script>
        <script src="public/js/script.js" type="text/javascript"></script>  
        
    </body>
</html>